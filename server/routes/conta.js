const express = require('express');
const router = express.Router();

const Conta = require('../models/usuario')
const Chamado = require('../models/chamado')

router.post('/novo', (req, res ) => {
    const novaConta = new Conta({
        cpf: '61214196012',
        cnpj: '27108418000173',
        pagamento: 'boleto'
    })
    const novaConta1 = new Conta({
        cpf: '33087179051',
        cnpj: '46733948000157',
        pagamento: 'boleto'
    })

    novaConta.save().then(result => { res.json(result)}).catch(error => { res.status(500).json(error)})
    novaConta1.save().then(result => { res.json(result)}).catch(error => { res.status(500).json(error)})
})

router.get('/', (req, res) => {
    Conta.find()
      .then(contas => {
        res.json(contas);
      })
      .catch(error => res.status(500).json(error));
  });


  router.put('/editar/:id', (req, res) => {
    const novosDados = { cpf: req.body.cpf, pagamento: req.body.pagamento };
    Conta.findOneAndUpdate({ _id: req.params.id }, novosDados, { new: true })
      .then(conta => {
        res.json(conta);
      })
      .catch(error => res.status(500).json(error));
  });
  
  
  router.delete('/delete/:id', (req, res) => {
    Conta.findOneAndDelete({ _id: req.params.id })
      .then(conta => {
        res.json(conta);
      })
      .catch(error => res.status(500).json(error));
  });

  router.get('/findAccount/:id', (req, res) => {
    Conta.exists({$or: [{cpf : req.params.id} , {cnpj: req.params.id}]}).then( conta => { 
      res.json(conta)
    }).catch(error => res.status(500).json(error))
  })

  router.delete('/deleteAll', async (req, res) => {
    Conta.deleteMany({}, function (err){
      if(err){
        console.log(err)
      }else{
        res.end('sucess')
      }
    });

  })


  router.post('/openCall', (req, res) => {
    const chamado = new Chamado({
      numero: Math.random() * 100,
      tipo: 'boleto',
      status: 'pendente',
      date: '25/10/2021',
      resolucao: '28/10/2021',
      cpfConta: '61214196012',
      cnpjConta: '27108418000173'
    })
    chamado.save().then(result => { res.json(result)}).catch(error => { res.status(500).json(error)})
  })

  router.get('/callData/:id',(req, res) => {
    Chamado.findOne({cpf: req.params.id }).then(chamado => {
      res.json(chamado)
    }).catch(error => res.status(500).json(error))
  })


module.exports = router;