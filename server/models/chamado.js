const mongoose = require('mongoose');
const { Schema } = mongoose;




const chamado = new Schema({
    numero: {
        type: String,
        require: true
    },
    tipo: {
        type: String,
        require: true
    },
    status:{
        type: String,
    },
    date:{
        type: String,
        require: true
    },
    resolucao:{
        type: String,
        require: true
    },
    cpfConta:{
        type: String,
        require: true
    },
    cnpjConta: {
        type: String,
        require: true
    }
})








module.exports = mongoose.model('chamado',chamado)