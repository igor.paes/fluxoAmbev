const mongoose = require('mongoose');
const { Schema } = mongoose;

const conta = new Schema({
    cpf: {
        type: String,
        require: true
    },
    cnpj: {
        type: String,
        require: true
    },
    pagamento: {
        type: String,
        require: true
    }
})

module.exports = mongoose.model('conta',conta)