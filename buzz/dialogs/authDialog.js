// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const { LuisRecognizer } = require('botbuilder-ai');
const {
    ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog, ChoicePrompt,
    ChoiceFactory, ListStyle
} = require('botbuilder-dialogs');
const axios = require('axios');

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';
const TEXT_PROMPT = 'TEXT_PROMPT';
const CHOICE_PROMPT = 'CHOICE_PROMPT';
const AUTH_PROMPT = 'AUTH_PROMPT';
const RESULT_PROMPT = 'RESULT_PROMPT';

class AuthDialog extends ComponentDialog {
    constructor(luisRecognizer) {
        super('AuthDialog');

        /* if (!luisRecognizer) throw new Error('[MainDialog]: Missing parameter \'luisRecognizer\' is required');
        this.luisRecognizer = luisRecognizer; */

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.
        this.addDialog(new TextPrompt(TEXT_PROMPT));
        this.addDialog(new TextPrompt(AUTH_PROMPT));
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ChoicePrompt(RESULT_PROMPT))
            .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
                this.introStep.bind(this),
                this.actionStep.bind(this)
            ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    async introStep(stepContext) {
        /* if (!this.luisRecognizer.isConfigured) {
            const messageText = 'NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file.';
            await stepContext.context.sendActivity(messageText, null, InputHints.IgnoringInput);
            return await stepContext.next();
        } */

        return stepContext.prompt(CHOICE_PROMPT, {
            prompt: 'Como posso te ajudar?\n\n',
            choices: ChoiceFactory.toChoices(['Mudar forma de pagamento', 'Consultar o status de um chamado']),
            style: ListStyle.list
        });
    }

    async actionStep(stepContext) {
        stepContext.values.secondChoice = stepContext.result;

        if (stepContext.values.secondChoice.value === 'consultar o status de um chamado' || stepContext.values.secondChoice.value === '2') {
            await stepContext.context.sendActivity('Fluxo ainda não implementado');
            return stepContext.beginDialog();
        } else {
            return stepContext.beginDialog('PaymentDialog');
        }
    }
}

module.exports.AuthDialog = AuthDialog;
