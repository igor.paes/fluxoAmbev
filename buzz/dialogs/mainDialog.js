// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const { LuisRecognizer } = require('botbuilder-ai');
const {
    ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog, ChoicePrompt,
    ChoiceFactory, ListStyle
} = require('botbuilder-dialogs');
const axios = require('axios');

var CPF = require('cpf_cnpj').CPF;
const CNPJ = require('@fnando/cnpj/commonjs');

const { MessageFactory } = require('botbuilder');
const { AuthDialog } = require('./authDialog');
const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';
const TEXT_PROMPT = 'TEXT_PROMPT';
const CHOICE_PROMPT = 'CHOICE_PROMPT';
const AUTH_PROMPT = 'AUTH_PROMPT';
const RESULT_PROMPT = 'RESULT_PROMPT';

class MainDialog extends ComponentDialog {
    constructor(luisRecognizer) {
        super('MainDialog');

        /* if (!luisRecognizer) throw new Error('[MainDialog]: Missing parameter \'luisRecognizer\' is required');
        this.luisRecognizer = luisRecognizer; */

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.
        this.addDialog(new TextPrompt(AUTH_PROMPT));
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ChoicePrompt(RESULT_PROMPT))
            .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
                this.introStep.bind(this),
                this.actionStep.bind(this),
                this.resultStep.bind(this),
                this.lastStep.bind(this)
            ]));
        this.addDialog(new AuthDialog(luisRecognizer));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    /**
     *
     * @param stepContext
     * @returns {Promise<*>} - retorna o input com a seguinte mensagem
     */
    async introStep(stepContext) {
        /* if (!this.luisRecognizer.isConfigured) {
            const messageText = 'NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file.';
            await stepContext.context.sendActivity(messageText, null, InputHints.IgnoringInput);
            return await stepContext.next();
        } */
        const msg = 'Oi 😀        ' +
            '\nEu sou o **Buzz** seu parceiro Virtual\n\n Conte comigo para:\n\n' +
            '\n🤳🏻 Informações Sobre Pedidos ou chamados    ' +
            '\n🛠 Manutenção de equipamentos   ' +
            '\n📄 Abrir Solicitações   ' +
            '\n\n E muito mais!';
        await stepContext.context.sendActivity(msg);
        this.createAccounts();
        return stepContext.prompt(AUTH_PROMPT, {
            prompt: 'Por favor, me informe o número do **CNPJ** ou do **CPF** que está no seu cadastro '
        });
    }

    /**
     *
     * @param stepContext
     * @returns {Promise<*|*>} - retorna a escolha do usuario ou autentica um usuario
     */
    async actionStep(stepContext) {
        stepContext.values.auth = stepContext.result;
        const auth = await this.handleAuth(stepContext.values.auth, stepContext);
        console.log(auth);
        if (auth === true) {
            const result = await this.searchAccount(stepContext.values.auth);
            if (result === true) {
                return await stepContext.beginDialog('AuthDialog');
            } else {
                await stepContext.context.sendActivity('Não encontrei seu cadastro 😔 ');
                return stepContext.prompt(CHOICE_PROMPT, {
                    prompt: 'Você já é nosso cliente?',
                    choices: ChoiceFactory.toChoices(['sim', 'nao']),
                    style: ListStyle.inline
                });
            }
        } else {
            return stepContext.beginDialog(this.id);
        }
    }

    /**
     *
     * @param stepContext
     * @returns {Promise<*>} - retorna uma escolha  ou retorna o cpf ou cnpj do usuario
     */
    async resultStep(stepContext) {
        stepContext.values.choice = stepContext.result;
        if (stepContext.values.choice.value === 'sim') {
            return stepContext.prompt(AUTH_PROMPT, {
                prompt: 'Por favor, confere aí o número do CNPJ ou do CPF e me manda novamente 🔎'
            });
        }
        return stepContext.prompt(CHOICE_PROMPT, {
            prompt: 'Aqui só fui treinado pra tirar dúvidas dos nossos clientes.               ' +
                    '\nMas podemos resolver isso agora!  😀 \n\n Você quer ser nosso parceiro?',
            choices: ChoiceFactory.toChoices(['sim', 'nao']),
            style: ListStyle.inline
        });
    }

    /***
     *
     * @param stepContext
     * @returns {Promise<*|*>} - valida novamente o cnpj ou cpf do usuario ou verifica se o usuario gostaria de se cadastrar ou não
     */
    async lastStep(stepContext) {
        stepContext.values.end = stepContext.result;
        console.log(stepContext.values.end);

        if (stepContext.values.end.length == 11 || stepContext.values.end.length == 14) {
            const value = await this.handleAuth(stepContext.values.end, stepContext);
            const result = await this.searchAccount(stepContext.values.end);
            if (value === true && result === true) {
                console.log(result);
                await stepContext.context.sendActivity('Pronto, encontrei seu cadastro 😉');
                return await stepContext.beginDialog('AuthDialog');
            } else {
                await stepContext.context.sendActivity('Sinto muito, por aqui ainda não encontrei seu cadastro 😔     ' +
                    '\nPor favor, entre em contato com nossa equipe 📞 0800 887 1111' +
                    ' que eles vão encontrar seu dados no sistema pra te ajudar!');
                return stepContext.endDialog(this.id);
            }
        } else {
            this.msg(stepContext);
            return stepContext.endDialog(this.id);
        }
    }

    /**
     *
     * @param step - valor do cpf ou cnpj
     * @param stepContext
     * @returns {Promise<boolean>} - retorna true para validações que passaram e false para validações que estão erradas
     */
    async handleAuth(step, stepContext) {
        const value = step;

        if (value.length === 11) {
            const cpfFormatted = CPF.format(value);
            const valid = CPF.isValid(cpfFormatted);
            if (valid === true) {
                return true;
            } else {
                await stepContext.context.sendActivity('CPF INVÁLIDO ');
                return false;
            }
        } else if (value.length === 14) {
            const cnpjFormatted = CNPJ.format(value);
            console.log('cnpj-> ' + cnpjFormatted);
            const valid = CNPJ.isValid(cnpjFormatted);

            if (valid === true) {
                return true;
            } else {
                await stepContext.context.sendActivity('CNPJ INVÁLIDO ');
                return false;
            }
        }
        return false;
    }

    /**
     *
     * @param value - recebe o valor do cnpj ou cpf para a busca no banco
     * @returns {Promise<unknown>} - retorna true se o cpf ou cnpj existe e false caso não exista
     */
    async searchAccount(value) {
        const response = await axios.get(`http://localhost:9000/api/conta/findAccount/${ value }`).then(function(response) {
            return response.data;
        }).catch(function(error) {
            console.log(error);
        });
        return response;
    }

    async createAccounts() {
        await axios.delete('http://localhost:9000/api/conta/deleteAll').then(function(response) {
        }).catch(function(error) {
            console.log(error);
        });

        await axios.post('http://localhost:9000/api/conta/novo').then(function(response) {
            return response.data;
        }).catch(function(error) {
            console.log(error);
        });
    }

    async msg(stepContext) {
        if (stepContext.values.end.value === 'sim') {
            await stepContext.context.sendActivity('Que notícia boa!😔              ' +
                '\nAgora é só acessar este link que a gente faz seu cadastro.\n\n' +
                'http://bit.ly/cadastroBees');
            return stepContext.endDialog(this.id);
        } else if (stepContext.values.end.value === 'nao') {
            await stepContext.context.sendActivity('Que pena 😔                           ' +
                '\nRealmente só consigo ajuda quem já é nosso cliente.\n\n' +
                'Se mudar de ideia é só me mandar uma mensagem por aqui! 😉');

        }
    }
}

module.exports.MainDialog = MainDialog;
