// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
const { LuisRecognizer } = require('botbuilder-ai');
const {
    ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog, ChoicePrompt,
    ChoiceFactory, ListStyle
} = require('botbuilder-dialogs');
const axios = require('axios');

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';
const TEXT_PROMPT = 'TEXT_PROMPT';
const CHOICE_PROMPT = 'CHOICE_PROMPT';
const AUTH_PROMPT = 'AUTH_PROMPT';
const RESULT_PROMPT = 'RESULT_PROMPT';

class PaymentDialog extends ComponentDialog {
    constructor(luisRecognizer) {
        super('PaymentDialog');

        /* if (!luisRecognizer) throw new Error('[MainDialog]: Missing parameter \'luisRecognizer\' is required');
        this.luisRecognizer = luisRecognizer; */

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.
        this.addDialog(new TextPrompt(TEXT_PROMPT));
        this.addDialog(new TextPrompt(AUTH_PROMPT));
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ChoicePrompt(RESULT_PROMPT))
            .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
                this.introStep.bind(this),
                this.choiceCall.bind(this),
                this.paymentStep.bind(this),
                this.dataStep.bind(this),
                this.actionStep.bind(this)
            ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    async introStep(stepContext) {
        /* if (!this.luisRecognizer.isConfigured) {
            const messageText = 'NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file.';
            await stepContext.context.sendActivity(messageText, null, InputHints.IgnoringInput);
            return await stepContext.next();
        } */

        const data = await this.searchForCall(stepContext.values.end);

        if (data === undefined) {
            return stepContext.prompt(TEXT_PROMPT, {
                prompt: 'Como você quer pagar seus pedidos: **boleto**, **cheque** ou **dinheiro**?'
            });
        } else {
            await stepContext.context.sendActivity('Vi aqui que você já tem um chamado aberto para alterar a forma ou o prazo de pagamento dos seus pedidos');
            await stepContext.context.sendActivity(`Numero de protocolo do chamado: ${ data.numero }
                                                                \nTipo de chamado: ${ data.tipo } 
                                                                \nStatus: ${ data.status } 
                                                                \nCriado em: ${ data.date } 
                                                                \nResolução prevista para: ${ data.resolucao }`);
            await stepContext.context.sendActivity('O prazo para analisar o seu chamado está um pouco maior que o normal, mas nosso time está se forçando para te dar um retorno logo');

            return stepContext.prompt(CHOICE_PROMPT, {
                prompt: 'Você precisa de ajuda com essa solicitação en andamento?\n\n',
                choices: ChoiceFactory.toChoices(['sim', 'nao']),
                style: ListStyle.list
            });
        }
    }

    async paymentStep(stepContext) {
        stepContext.values.payment = stepContext.result;
        if (stepContext.result === 'boleto' || stepContext.result === 'cheque' || stepContext.result === 'dinheiro') {

        } else if (stepContext.result === 'credito' || stepContext.result === 'debito') {
            return stepContext.context.sendActivity('Realmente não consigo alterar a forma de pagamento dos seus pedidos para a opção desejada');
        }
        await stepContext.context.sendActivity('Ainda não aceitamos pagamento com cartão ☹');
        await stepContext.context.sendActivity('Você pode pagar seus pedidos com:\n\n  ' +
                                                          '\n1 - Boleto  ' +
                                                          '\n2- Cheque   ' +
                                                          '\n3 - Dinheiro  \n\n');
        return stepContext.prompt(TEXT_PROMPT, 'Qual dessas formas de pagamento você prefere?');
    }

    async choiceCall(stepContext) {
        if (stepContext.result === 'sim') {
            return stepContext.context.sendActivity('Para resolver isso, você pode contar com a ajuda da nossa equipe no app do **Parceiro Ambev**👉' +
               'http://onelink.to/4u2bf4 ou no 📞 0800 887 1111. \n\n' +
               'Se eu puder ajudar com outras solicitações é só mandar uma mensagem por aqui!👋');
        }

        return stepContext.context.sendActivity('Tudo Bem! 😀');
    }

    async dataStep(stepContext) {
        stepContext.value.paymentConfirmation = stepContext.result;
        return stepContext.prompt(TEXT_PROMPT, {
            prompt: 'Qual o prazo de pagamento desejado?🤔'
        });
    }

    async actionStep(stepContext) {
        stepContext.value.date = stepContext.result;
        if (stepContext.value.date >= 1 && stepContext.value.date <= 7) {
            return stepContext.context.sendActivity('Já estou abrindo um chamado para que o nosso time financeiro avalie sua solicitação 😉');
        } else if (stepContext.value.date >= 8) {
            return stepContext.prompt(TEXT_PROMPT, {
                prompt: 'Você pode pedir até 7 dias para para seus pedidos. Por favor, informe o prazo desejado.'
            });
        }
    }

    async finalStep(stepContext) {
        if (stepContext.result) {
            stepContext.value.final = stepContext.result;
            if (stepContext.value.final >= 8) {
                return stepContext.context.sendActivity('Esse prazo não é um prazo possível 😔');
            }
        }
        // api responde stepContext.value.date
    }

    async searchForCall(value) {
        await axios.post('http://localhost:9000/api/conta/openCall').then(function(response) {
        }).catch(function(error) {
            console.log(error);
        });

        const data = await axios.get(`http://localhost:9000/api/conta/callData/${ value }`).then(function(response) {
            return response.data;
        }).catch(function(error) {
            console.log(error);
        });
        return data;
    }
}

module.exports.PaymentDialog = PaymentDialog;
